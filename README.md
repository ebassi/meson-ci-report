A small collection of scripts to be used with Meson and GitLab's CI pipelines
in order to generate reports based on the test suite results and logs.

## Contents

 - `meson-junit-report.py`: Generate a [JUnit XML][junit] report from the
   Meson tests log
 - `meson-html-report.py`: Generate an HTML report from the Meson test log

[junit]: https://github.com/windyroad/JUnit-Schema

## Usage

We assume you already have a GitLab CI pipeline building your project and
running the tests with Meson.

To build the test reports you will need to run the appropriate script, for
instance:

```sh
python3 meson-junit-report.py \
        --project-name=your-project \
        --job-id="${CI_JOB_NAME}" \
        --output=report.xml \
        meson-logs/testlog.json
```

As you may want to generate the reports even on failure, you will need to
have a single script that runs `meson test` and the report scripts.
Alternatively, you can run the report scripts in the `.post` stage, making
sure to store the Meson logs inside the artifacts cache.

### Adding a JUnit report to merge requests

Add this fragment to the appropriate job definition in you `.gitlab-ci.yml`
file:

```
  artifacts:
    when: always
    reports:
      junit:
        - "${CI_PROJECT_DIR}/report.xml"
```

and generate the `report.xml` file using:

```
python3 meson-junit-report.py \
        --project-name=your-project \
        --job-id="${CI_JOB_NAME}" \
        --output=report.xml \
        _build/meson-logs/testlog.json
```
